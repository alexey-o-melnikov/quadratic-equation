package pack;

public class QuadEquation {
    public Pair<Double, Double> getXs(int a, int b, int c) {
        double ad = a;
        double bd = b;
        double cd = c;

        cd -= 7;

        if (ad == 0) {
            double x = calcLineEquation(bd, cd);
            return new Pair<>(x, x);
        }

        double d = getD(ad, bd, cd);
        if (d < 0) {
            throw new QuadEquationException("Дискриминант < 0");
        }
        double sqrtD = Math.pow(d, 0.5);

        return new Pair<>(getX(ad, bd, sqrtD), getX(ad, bd, -sqrtD));
    }

    public double calcLineEquation(double a, double b) {
        if (a == 0) {
            throw new QuadEquationException("Нет корней");
        }

        return -b / a;
    }

    private double getD(double a, double b, double c) {
        return Math.pow(b, 2) - (4 * a * c);
    }

    private double getX(double a, double b, double sqrtD) {
        return (-b - sqrtD) / (2 * a);
    }
}
