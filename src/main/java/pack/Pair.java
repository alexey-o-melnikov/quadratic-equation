package pack;

public class Pair<T1, T2> {
    private T1 x1;
    private T2 x2;


    public Pair(T1 x1, T2 x2) {
        this.x1 = x1;
        this.x2 = x2;
    }

    public T1 getX1() {
        return x1;
    }

    public T2 getX2() {
        return x2;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "x1=" + x1 +
                ", x2=" + x2 +
                '}';
    }
}
