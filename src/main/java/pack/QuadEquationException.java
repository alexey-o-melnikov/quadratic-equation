package pack;

public class QuadEquationException extends RuntimeException {
    public QuadEquationException(String message) {
        super(message);
    }
}
