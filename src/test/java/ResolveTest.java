import org.junit.Assert;
import org.junit.Test;
import pack.Pair;
import pack.QuadEquation;
import pack.QuadEquationException;

public class ResolveTest {

    @Test(expected = QuadEquationException.class)
    public void allZero() {
        int a = 0;
        int b = 0;
        int c = 0;

        QuadEquation quadEquation = new QuadEquation();
        Pair<Double, Double> xs = quadEquation.getXs(a, b, c);
    }

    @Test
    public void aIsZero() {
        int a = 0;
        int b = 1;
        int c = 1;

        QuadEquation quadEquation = new QuadEquation();
        Pair<Double, Double> xs = quadEquation.getXs(a, b, c);

        Assert.assertEquals(xs.getX2(), xs.getX1());
        Assert.assertEquals(new Double(6.0), xs.getX1());
    }

    @Test(expected = QuadEquationException.class)
    public void allMax() {
        int a = Integer.MAX_VALUE;
        int b = Integer.MAX_VALUE;
        int c = Integer.MAX_VALUE;

        QuadEquation quadEquation = new QuadEquation();
        Pair<Double, Double> xs = quadEquation.getXs(a, b, c);
    }

    @Test(expected = QuadEquationException.class)
    public void allMin() {
        int a = Integer.MIN_VALUE;
        int b = Integer.MIN_VALUE;
        int c = Integer.MIN_VALUE;

        QuadEquation quadEquation = new QuadEquation();
        Pair<Double, Double> xs = quadEquation.getXs(a, b, c);
    }

    @Test
    public void allOne() {
        int a = 1;
        int b = 1;
        int c = 1;

        QuadEquation quadEquation = new QuadEquation();
        Pair<Double, Double> xs = quadEquation.getXs(a, b, c);

        Assert.assertEquals(new Double(-3.0), xs.getX1());
        Assert.assertEquals(new Double(2.), xs.getX2());
    }

    @Test
    public void discriminantIsZero() {
        int a = 1;
        int b = 2;
        int c = 8;

        QuadEquation quadEquation = new QuadEquation();
        Pair<Double, Double> xs = quadEquation.getXs(a, b, c);

        Assert.assertEquals(xs.getX2(), xs.getX1());
    }

    @Test(expected = QuadEquationException.class)
    public void calcLineEquationAllZeroTest() {
        double a = 0;
        double b = 0;

        QuadEquation quadEquation = new QuadEquation();
        double x = quadEquation.calcLineEquation(a, b);
    }
}
